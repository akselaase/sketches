from matplotlib import pyplot as plt
import numpy as np
import numba
import time


def _init_grid(lower_left, upper_right, resolution):
    x_axis = np.linspace(lower_left[0], upper_right[0], resolution[0])
    y_axis = np.linspace(lower_left[1], upper_right[1], resolution[1])
    xv, yv = np.meshgrid(x_axis, y_axis)
    return xv + 1j * yv


@numba.vectorize([numba.boolean(numba.complex128), numba.boolean(numba.complex64)])
def _has_escaped(x):
    return (x.real**2 + x.imag**2) >= 4


@numba.vectorize([numba.complex128(numba.complex128, numba.complex128), numba.complex64(numba.complex64, numba.complex64)])
def _iter(z, c):
    return z * z + c


def mandelbrot(lower_left, upper_right, resolution, iterations):
    grid = _init_grid(lower_left, upper_right, resolution)
    C = grid.reshape(-1)
    N = np.zeros(C.shape[0])
    I = np.arange(C.shape[0])
    Z = np.copy(C)
    for i in range(iterations):
        Z = _iter(Z, C)
        exterior = _has_escaped(Z)
        interior = np.logical_not(exterior)
        N[I[exterior]] = i + 1
        Z = Z[interior]
        C = C[interior]
        I = I[interior]
        if Z.size == 0:
            break
    return N.reshape(grid.shape)


def show(res=500, i=100):
    ll = np.array([-3, -2])
    tr = np.array([2, 2])
    res = (tr - ll) * res
    N = mandelbrot(ll, tr, res, i)
    plt.imshow(N)
    plt.show()


def bench(res=500, i=100, c=100):
    ll = np.array([-3, -2])
    tr = np.array([2, 2])
    res = (tr - ll) * res

    ts = time.time_ns()
    for i in range(c):
        mandelbrot(ll, tr, res, i)
    print((time.time_ns() - ts) / c)


plt.gray()

show()
