from p5 import *
import time


class State:
    def __init__(self, inner_state, outer_state):
        self.m_inner, self.p_inner, self.v_inner = inner_state
        self.m_outer, self.p_outer, self.v_outer = outer_state
        self.wall_hits = 0
        self.box_hits = 0

        sum_m = self.m_inner + self.m_outer
        diff_m = self.m_inner - self.m_outer
        self._ico1 = diff_m / sum_m
        self._ico2 = 2 * self.m_outer / sum_m
        self._oco1 = 2 * self.m_inner / sum_m
        self._oco2 = -diff_m / sum_m

    def tick(self, dt):
        inner = self.p_inner + dt * self.v_inner
        outer = self.p_outer + dt * self.v_outer
        self.set_pos(inner, outer)

    def set_pos(self, inner, outer):
        self.p_inner = inner
        self.p_outer = outer
        if self.p_outer <= self.p_inner:
            # The inner and outer box collided
            self.box_hits += 1
            # New velocities
            vel_inner = self.v_inner * self._ico1 + self.v_outer * self._ico2
            vel_outer = self.v_inner * self._oco1 + self.v_outer * self._oco2
            self._resolve_overlap(vel_inner, vel_outer)
            self.v_inner = vel_inner
            self.v_outer = vel_outer
        if self.p_inner <= 0:
            self.p_inner = -self.p_inner
            self.v_inner = -self.v_inner
            self.wall_hits += 1

    def done(self):
        return self.v_outer > 0 and abs(self.v_inner) < self.v_outer

    def _resolve_overlap(self, vnew_inner, vnew_outer):
        pass


def color(*args, **kwargs):
    stroke(*args, **kwargs)
    fill(*args, **kwargs)


def setup():
    time.prev = time.time()
    size(400, 480)


state = State((1, 0.5, 0), (100, 0.9, -0.05))
time_scale = 1
draw_scale = 400
vertices = []


def draw():
    dt, time.prev = time.time() - time.prev, time.time()
    dt *= time_scale

    state.tick(dt)
    vertices.append((state.p_inner * draw_scale,
                     480 - state.p_outer * draw_scale))
    print(state.box_hits + state.wall_hits)

    background(255)
    color(255, 0, 0)
    rect((state.p_inner * draw_scale, 0), 2, 80)
    color(0, 255, 0)
    rect((state.p_outer * draw_scale, 0), 2, 80)

    color(0, 0, 0)
    for first, last in zip(vertices, vertices[1:]):
        line(first, last)
    circle(last, 5)
    line((0, 480), (400, 80))


run()
