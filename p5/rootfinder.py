from p5 import *
import math
import cmath


wx, wy = 640, 640
scale = 100
value = 1 + 0j
num_roots = 3
roots = []


def c2_to_s(re, im):
    px = re * scale + wx / 2
    py = wy / 2 - im * scale
    return px, py


def c_to_s(value):
    return c2_to_s(value.real, value.imag)


def s_to_c2(pixel):
    real = (pixel[0] - wx / 2) / scale
    imag = (wy / 2 - pixel[1]) / scale
    return real, imag


def s_to_c(pixel):
    return complex(*s_to_c2(pixel))


def setup():
    size(wx, wy)


def draw():
    if roots:
        background(255)
        fill(0)
        draw_grid()
        for root in roots:
            circle(c_to_s(root), 5)
        roots.clear()


def draw_grid():
    stroke(0)
    min_re, min_im = s_to_c2((0, wy))
    max_re, max_im = s_to_c2((wx, 0))
    for r in range(ceil(min_re), ceil(max_re)):
        line(c2_to_s(r, min_im), c2_to_s(r, max_im))
    for i in range(ceil(min_im), ceil(max_im)):
        line(c2_to_s(min_re, i), c2_to_s(max_re, i))


def calc_roots():
    roots.clear()
    mag, phase = cmath.polar(value)
    rmag = math.pow(mag, 1/num_roots)
    for i in range(0, num_roots):
        rphase = (phase + 2 * i * math.pi) / num_roots
        roots.append(cmath.rect(rmag, rphase))


def key_pressed(event):
    global num_roots
    if str(event.key) in '123456789':
        num_roots = int(str(event.key))
        calc_roots()


def mouse_moved(event):
    global value
    value = s_to_c((event.x, event.y))
    calc_roots()


calc_roots()
run()
